import { useRef, useState } from 'react';
import './App.css';

function Square({ value, onSquareClick, idName }) {
  return (
    <button id={idName} className='square' onClick={onSquareClick}>{value}</button>);
}

function Board({ xIsNext, squares, onPlay, movesNb }) {

  const winner = calculateWinner(squares);
  let drawState = (movesNb === 10) ? true : false;
  let status;
  if (winner) {
    winner[1].forEach(element => {
      document.getElementById('square' + element).style.backgroundColor = "lime";
    });
    status = "Winner: " + winner[0];
  } else if (drawState === false) {
    status = "Next player: " + (xIsNext ? "X" : "O");
    clearBackgrounds();
  } else {
    status = "Draw";
    clearBackgrounds();
  }

  function clearBackgrounds() {
    const squares = document.getElementsByClassName("square");

    Array.from(squares).forEach(element => {
      element.style.backgroundColor = "white";
    });
  }

  function handleClick(i) {
    if (squares[i] || calculateWinner(squares)) {
      return;
    }
    const nextSquares = squares.slice();
    const lastMoveCoord = "(" + (Math.floor(i / 3) + 1) + "," + (i % 3 + 1) + ")";
    if (xIsNext) {
      nextSquares[i] = "X";
    } else {
      nextSquares[i] = "O";
    }

    onPlay(nextSquares, lastMoveCoord);
  }

  function boardRow(j) {
    const elements = [];
    for (let i = 0; i < 3; i++) {
      elements.push(<Square idName={'square' + (3 * j + i)} key={'square' + (3*j+i)} value={squares[3 * j + i]} onSquareClick={() => handleClick(3 * j + i)} />)
    }

    return (
      <div key={'row' + j} className="board-row">
        {elements}
      </div>
    )
  }

  const boardRows = []
  for (let j = 0; j < 3; j++) {
    boardRows.push(boardRow(j));
  }

  return (
    <>
      <div className='status'>{status}</div>
      {boardRows}
    </>
  );
}

function Game() {
  const [history, setHistory] = useState({
    grid: [Array(9).fill(null)],
    coord: []
  });
  const [currentMove, setCurrentMove] = useState(0);
  const [order, setOrder] = useState(true);
  const xIsNext = currentMove % 2 === 0;
  const currentSquares = history.grid[currentMove];

  function handlePlay(nextSquares, coord) {
    const nextHistory = {
      grid: [...history.grid.slice(0, currentMove + 1), nextSquares],
      coord: [...history.coord.slice(0, currentMove + 1), coord]
    };
    setHistory(nextHistory);
    setCurrentMove(nextHistory.grid.length - 1);
  }

  function jumpTo(nextMove) {
    setCurrentMove(nextMove);
  }

  function reverseOrder(order) {
    setOrder(!order);
  }


  const moves = history.grid.map((bloop, move) => {
    let description;

    // console.log(history.coord[move]);

    if (move === currentMove && move !== 0) {
      return (
        <li key="current">
          <p>{'Current move #' + move + ' ' + history.coord[move-1]}</p>
        </li>
      );
    }
    if (move > 0) {
      description = 'Go to move #' + move + ' ' + history.coord[move-1];
    } else {
      description = 'Go to game start';
    }
    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{description}</button>
      </li>
    );
  });

  let orderedMoves = (order ? moves : moves.reverse());

  return (
    <div className='game'>
      <div className='game-board'>
        <Board xIsNext={xIsNext} squares={currentSquares} onPlay={handlePlay} movesNb={moves.length} />
      </div>
      <div className='game-info'>
        <ol>
          <li>
            <button key='button' onClick={() => reverseOrder(order)}>Sort</button>
          </li>
          {orderedMoves}
        </ol>
      </div>
    </div>
  );
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return [squares[a], lines[i]];
    }
  }
  return null;
}

export default Game;
